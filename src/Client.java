import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;


public class Client {
	
	public Client(String host, int port){
		try {
			Socket conn = new Socket(host, port);
			DataInputStream dis = new DataInputStream(conn.getInputStream());
			DataOutputStream dos = new DataOutputStream(conn.getOutputStream());
			Person[] datensatz = { new Person(1), new Person(2), new Person(0), new Person (1)};
			
			dos.writeInt(datensatz.length);
			
			for (int i = 0; i < datensatz.length; i++) {
				dos.writeInt(datensatz[i].kategorie);

			}
			int matrosen = dis.readInt();
			int passagiere = dis.readInt();
			
			System.out.println(matrosen);
			System.out.println(passagiere);
			
			dis.close();
			dos.close();
			conn.close();
		} catch(Exception e) {
			e.printStackTrace(System.err);
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Client("localhost", 4711);
	}

}
