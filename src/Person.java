
public class Person {
	public final static int KAPITAEN = 0;
	public final static int MATROSE = 1;
	public final static int PASSAGIER = 2;
	
	public long id;
	public String name;
	public int kategorie;
	
	Person(int kategorie){
		this.kategorie = kategorie;
	}
}
