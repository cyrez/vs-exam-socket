import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;


public class Server {


	public void start(int port) {
		try{
			ServerSocket server = new ServerSocket(port);
			Socket conn = null;
			while(true) {
				try{
					conn = server.accept();
				} catch(Exception e){
					e.printStackTrace(System.err);
				}
				DataInputStream din = new DataInputStream(conn.getInputStream());
				DataOutputStream dos = new DataOutputStream(conn.getOutputStream());
				
				int matrosen = 0;
				int passagiere = 0;
				int count = din.readInt();
				for (int i = 0; i < count; i++) {
					switch (din.readInt()) {
					case 1:
						matrosen++;
						break;
					case 2:
						passagiere++;
					default:
						break;
					}
				}
				dos.writeInt(matrosen);
				dos.writeInt(passagiere);
				
				din.close();
				dos.close();
				conn.close();
			}
		} catch (IOException e){
			e.printStackTrace(System.err);
		}
	}
	
	public static void main(String[] args) {
		new Server().start(4711);
	}

}

